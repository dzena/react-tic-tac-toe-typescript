import {MouseEvent} from "react";
import React from "react";

type SquareProps = {
    onClick(e: MouseEvent<HTMLElement>): void,
    value: string
};

export const Square: React.FunctionComponent<SquareProps> = ({onClick: handleClick, value}) => {
    return <button className="square" onClick={handleClick}>{value}</button>;
};