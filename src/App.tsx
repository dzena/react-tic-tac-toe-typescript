import React, {Component} from 'react';
import './App.scss';
import {Game} from "./game/game";

class App extends Component {
    render() {
        return <Game history={[]} stepNumber={0} xIsNext={true}/>
    }
}

export default App;
